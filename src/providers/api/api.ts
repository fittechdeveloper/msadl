import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  public config = {
    clientID: '916d0a3e-8844-44a0-b90b-cd3a27c68701',
    authorizeURI: 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
    logoutURI: 'https://login.microsoftonline.com/common/oauth2/v2.0/logout',
    tokenURI: 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
    graphURI: 'https://graph.microsoft.com/v1.0/me',
    redirectURI: 'msal916d0a3e-8844-44a0-b90b-cd3a27c68701://auth',
    graphScopes: ['user.read', 'mail.send']
  }
  constructor(public http: HTTP) {
    console.log('Hello ApiProvider Provider');
  }

  getAuthorizeURL(){
    let params = {
      client_id: this.config.clientID,
      response_type: 'code',
      redirect_uri: this.config.redirectURI,
      response_mode: 'query',
      scope: this.config.graphScopes.join(' '),
      state: this.makeid(8)
    }
    return this.config.authorizeURI + "?" + this.queryString(params);
  }

  getRequest(url: string, parameters?: any, headers?: any){
    return this.http.get(url, parameters, headers);
  }

  postRequest(url: string, body?: any, headers?: any){
    return this.http.post(url, body, headers);
  }

  tokenRequest(tokenCode){
    const params = {
      client_id: this.config.clientID,
      scope: this.config.graphScopes.join(' '),
      code: tokenCode,
      redirect_uri: this.config.redirectURI,
      grant_type: 'authorization_code'
    }
    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    return this.postRequest(this.config.tokenURI, params, headers)
  }

  graphRequest(accessToken){
    const header ={
      'Authorization': 'Bearer ' + accessToken
    };
    return this.getRequest(this.config.graphURI, null, header)
  }

  private queryString(params){
    return Object.keys(params).map(function(key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
    }).join('&');
  }

  private makeid(length) {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

}
