import { Component } from '@angular/core';
import { NavController, LoadingController, Loading } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ApiProvider } from '../../providers/api/api';
import { HTTPResponse } from '@ionic-native/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loading: Loading;
  accessToken;
  user = {
    name: 'GUEST',
    email: '',
    logedIn: false
  }
  constructor(
    public navCtrl: NavController,
    private iab: InAppBrowser,
    private api: ApiProvider,
    private loadingCtrl: LoadingController
  ) {

  }

  login() {

    this.createLoader();
    this.loading.present().then(() => {
      const browser = this.iab.create(this.api.getAuthorizeURL(), '_blank', 'location=no, zoom=no');
      const subscriberOnExit = browser.on('exit').subscribe(() => {
        console.log("Sign in flow was canceled");;
        this.dismissLoading();
      });

      browser.on('loadstart').subscribe( async (event) => {
        try {
          let redirectUrl = this.api.config.redirectURI.indexOf('http://') === 0 ? this.api.config.redirectURI : 'http://' + this.api.config.redirectURI;
          if ((event.url).indexOf(redirectUrl) === 0) {
            console.log("Sign in success");;
            subscriberOnExit.unsubscribe();
            browser.close();
              // Here is some code that extracts a param from the full `event.url`
              const url = new URL(event.url);
              const tokenCode = url.searchParams.get('code');
              this.tokenRequestCallback(await this.api.tokenRequest(tokenCode));
          }
        } catch (error) {
          console.log(error);
          this.dismissLoading();
        }

      });
    })

  }

  logout(){
    this.createLoader();
    this.loading.present().then(async () => {
      await this.api.getRequest(this.api.config.logoutURI)
      this.accessToken = null;
      this.user.name = 'GUEST';
      this.user.email = '';
      this.user.logedIn = false;
      this.dismissLoading();
    });
  }

  async tokenRequestCallback(httpResponse: HTTPResponse) {
    try {
      if (!httpResponse.error) {
        const data = JSON.parse(httpResponse.data);
        console.log(data);
        this.accessToken = data.access_token;
        this.updateUI(await this.api.graphRequest(this.accessToken));
      } else {
        console.log(httpResponse.error);
        this.dismissLoading();
      }
    } catch (error) {
      console.log(error);
      this.dismissLoading();
    }
  }

  updateUI(httpResponse: HTTPResponse) {
    if (!httpResponse.error) {
      const data = JSON.parse(httpResponse.data || {});
      this.user.name = data.displayName || 'GUEST';
      this.user.email = data.mail || data.userPrincipalName || '';
      this.user.logedIn = this.user.email !== '';
    } else {
      console.log(httpResponse.error);
    }
    this.dismissLoading();
  }

  dismissLoading() {
    if (this.loading)
      this.loading.dismissAll();
  }

  createLoader() {
    this.loading = this.loadingCtrl.create({
      enableBackdropDismiss: false
    });
  }

}
